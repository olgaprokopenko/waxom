const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const concat = require('gulp-concat');
const pug = require('gulp-pug');
const browserSync = require('browser-sync');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const cleanCSS = require('gulp-clean-css');
 

gulp.task('image', function () {
    gulp.src('./src/assets/img/*')
        .pipe(image())
        .pipe(gulp.dest('./build/assets/img'));
});

gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'));
});

gulp.task('pug', function () {
    gulp.src('./src/pages/**/*.pug')
        .pipe(pug())
        .pipe(concat('index.html'))
        .pipe(gulp.dest('./build'));
});

gulp.task('sass', function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(concat('style.css'))
        // .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./build/styles'));
});

gulp.task('js', function () {
    gulp.src('./src/js/*.js')
        .pipe(gulp.dest('./build/js'));
});

gulp.task('vendors', function () {
    gulp.src('./vendors/**/**/*.*')
        .pipe(gulp.dest('./build/vendors'));
});

gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: './build/'
        }
    });
});

gulp.task('fonts', function () {
    return gulp.src('./src/assets/fonts/**/*')
        .pipe(gulp.dest('./build/assets/fonts'))
});

gulp.task('clean', function () {
    return del.sync('build');
  });

gulp.task('watch', ['sass', 'pug', 'html', 'fonts', 'image', 'clean', 'browserSync', 'js', 'vendors'], function () {
    gulp.watch('./src/styles**/*.scss', ['sass']);
    gulp.watch('./src/assets/img/*', ['image']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./src/pages/**/*.pug', ['pug']);
    gulp.watch('./src/js/*.js', ['js']);
    gulp.watch('./vendors/**/**/*.*', ['vendors']);
    gulp.watch('./src/assets/fonts/**/*', ['fonts']);
    gulp.watch('build/*.html', browserSync.reload);
    gulp.watch("./build/**/*.css").on("change", browserSync.reload);
    gulp.watch('./build/**/*.js').on("change", browserSync.reload);
});

gulp.task('default', ['watch', 'image']);